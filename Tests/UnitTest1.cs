using FluentAssertions;
using Xunit;

namespace Tests
{
    public class UnitTest1
    {
        [Fact]
        public void Test1()
        {
            ClassLibrary.Class1.Hello().Should().Be("Hello world!");
        }

        [Fact]
        public void Test2()
        {
            ClassLibrary.Class2.Bye().Should().Be("Goodbye...");
        }
    }
}
