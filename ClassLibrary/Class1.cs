using System;

namespace ClassLibrary
{
    public class Class1
    {
        public static string Hello()
        {
            return @"Hello world!";
        }

        public static string HowAreYou()
        {
            return "Great!";
        }
    }
}
