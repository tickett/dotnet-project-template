# .NET Project Template

## Goal of this template

_TBD: The goal of this template is..._

## How to recreate

```shell
# This next line generates a solution with the same name as the containing folder
dotnet new sln
# To specify a custom name, provide the '--name <name>' argument
# dotnet new sln --name dotnet-project-template

dotnet new classlib -n ClassLibrary
dotnet new xunit -n Tests
dotnet add Tests/Tests.csproj reference ClassLibrary/ClassLibrary.csproj 
cd Tests
dotnet add package JunitXml.TestLogger
dotnet add package coverlet.console
dotnet add package FluentAssertions
dotnet add package FluentAssertions.Analyzers
cd ..
dotnet sln add ClassLibrary
dotnet sln add Tests
```